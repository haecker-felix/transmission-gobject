use std::cell::OnceCell;

use glib::object::ObjectExt;
use glib::subclass::prelude::{ObjectSubclass, *};
use glib::Properties;

mod imp {
    use super::*;

    #[derive(Debug, Default, Properties)]
    #[properties(wrapper_type = super::TrAuthentication)]
    pub struct TrAuthentication {
        #[property(get, set, construct_only)]
        pub username: OnceCell<String>,
        #[property(get, set, construct_only)]
        pub password: OnceCell<String>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for TrAuthentication {
        const NAME: &'static str = "TrAuthentication";
        type Type = super::TrAuthentication;
        type ParentType = glib::Object;
    }

    #[glib::derived_properties]
    impl ObjectImpl for TrAuthentication {}
}

glib::wrapper! {
    pub struct TrAuthentication(ObjectSubclass<imp::TrAuthentication>);
}

impl TrAuthentication {
    pub fn new(username: &str, password: &str) -> Self {
        glib::Object::builder()
            .property("username", username)
            .property("password", password)
            .build()
    }
}
