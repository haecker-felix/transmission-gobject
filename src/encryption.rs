use glib::Enum;
use transmission_client::Encryption;

#[derive(Default, Copy, Debug, Clone, PartialEq, Enum)]
#[repr(u32)]
#[enum_type(name = "TrEncrypption")]
pub enum TrEncryption {
    Required,
    #[default]
    Preferred,
    Tolerated,
}

impl From<u32> for TrEncryption {
    fn from(u: u32) -> Self {
        match u {
            0 => Self::Required,
            1 => Self::Preferred,
            2 => Self::Tolerated,
            _ => Self::default(),
        }
    }
}

impl From<Encryption> for TrEncryption {
    fn from(enc: Encryption) -> Self {
        match enc {
            Encryption::Required => TrEncryption::Required,
            Encryption::Preferred => TrEncryption::Preferred,
            Encryption::Tolerated => TrEncryption::Tolerated,
        }
    }
}

impl From<TrEncryption> for Encryption {
    fn from(val: TrEncryption) -> Self {
        match val {
            TrEncryption::Required => Encryption::Required,
            TrEncryption::Preferred => Encryption::Preferred,
            TrEncryption::Tolerated => Encryption::Tolerated,
        }
    }
}
