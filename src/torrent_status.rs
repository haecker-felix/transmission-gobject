use glib::Enum;
use num_enum::TryFromPrimitive;

#[derive(Default, Debug, Copy, Clone, Enum, PartialEq, TryFromPrimitive)]
#[repr(u32)]
#[enum_type(name = "TrTorrentStatus")]
pub enum TrTorrentStatus {
    #[default]
    Stopped = 0,
    CheckWait = 1,
    Check = 2,
    DownloadWait = 3,
    Download = 4,
    SeedWait = 5,
    Seed = 6,
}
